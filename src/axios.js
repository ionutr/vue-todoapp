import Vue from 'vue'
import axios from 'axios'
import store from "./store";
import VueAxios from 'vue-axios'

axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.withCredentials = true

const storedToken = localStorage.getItem(process.env.VUE_APP_TOKEN)
if (storedToken) {
	axios.defaults.headers.common['Authorization'] = 'Bearer ' + storedToken
}

// intercept responses, handle 500 & 401
axios.interceptors.response.use(undefined, (error) => {
  if (error.response) {
    if (401 === error.response.status) {
      store.commit('AUTH_LOGOUT')
    }

    return Promise.reject(error)
  }
})

Vue.use(VueAxios, axios)