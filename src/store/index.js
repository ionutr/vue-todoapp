import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
	  accessToken: localStorage.getItem(process.env.VUE_APP_TOKEN) || null,
	  dictionary: null,
	  lang: 'en', // 'eb' || 'de' || 'ro'
	  messages: [],
	  status: 'idle',
	  user: null
  },
  mutations: {
  	APP_DICTIONARY(state, messages) {
  		state.dictionary = messages
	  },
	  APP_LANG(state, language) {
  		state.lang = language
	  },
	  APP_MESSAGE(state, { message, type = 'info', options = {} }) {
		  state.messages.push({
		  	message: message,
			  type: type,
			  options: options
		  })
	  },
	  APP_USER(state, user) {
		  state.user = user
	  },
	  AUTH_LOGOUT(state) {
		  state.accessToken = false
		  state.user = null

		  localStorage.removeItem(process.env.VUE_APP_TOKEN)
		  delete Vue.axios.defaults.headers.common['Authorization']
	  },
	  AUTH_SUCCESS(state, token) {
		  state.status = 'success'
		  state.accessToken = token
	  },
	  REQUEST_INIT(state) {
		  state.status = 'loading'
	  },
	  REQUEST_STOP(state) {
		  state.status = 'idle'
	  },
	  RESET_MESSAGES(state) {
	  	state.messages = []
	  }
  },
  actions: {
	  AUTH_REQUEST: ({commit}, credentials) => {
		  return new Promise((resolve, reject) => {
			  commit('REQUEST_INIT')
			  // commit('ERROR_RESET')

			  Vue.axios.post('login', credentials, {
			  	withCredentials: true
				}).then((response) => {
				  const token = response.data.access_token,
						  user = response.data.user

				  localStorage.setItem(process.env.VUE_APP_TOKEN, token)
				  Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token

				  commit('AUTH_SUCCESS', token)
				  commit('APP_USER', user)
				  commit(
				  		'APP_MESSAGE',
						  {
							  message: 'Successfully logged in',
							  type: 'success',
							  options: {
								  icon: 'fas fa-info-circle'
							  }
						  }
		      )
				  commit('REQUEST_STOP')

				  resolve(response)

			  }).catch((error) => {
				  localStorage.removeItem(process.env.VUE_APP_TOKEN)
				  delete Vue.axios.defaults.headers.common['Authorization']

				  reject(error)
			  })
		  })
	  },
	  DEAUTH_REQUEST: ({commit}) => {
		  return new Promise((resolve, reject) => {

			  commit('REQUEST_INIT')
			  Vue.axios.get('logout').then(() => {
				  commit('AUTH_LOGOUT')
				  commit(
				  		'APP_MESSAGE',
						  {
						  	message: 'Successfully logged out'
						  }
				  )
				  commit('REQUEST_STOP')

				  resolve()
			  }).catch((error) => {
				  commit('AUTH_LOGOUT')
				  commit('REQUEST_STOP')

				  reject(error)
			  })
		  })
	  }
  },
  modules: {},
	getters: {
		isAuthenticated: state => !!state.accessToken,
		getCurrentUser: state => state.user,
		getDictionary: state => state.dictionary,
		status: state => state.status,
		token: state => state.accessToken
	}
});
