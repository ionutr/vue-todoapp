import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/add",
    name: "add-todo",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "addtodo" */ "../views/AddTodo.vue")
  },
	{
		path: "/edit/:id",
		name: "edit-todo",
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "edittodo" */ "../views/EditTodo.vue")
	},
	{
		path: '/register',
		name: 'register',
		meta: {
			guest: true
		},
		component: () =>
			import(/* webpackChunkName: "register" */ "../views/Register.vue")
	},
	{
		path: '/confirm/:token',
		name: 'confirm-email',
		meta: {
			guest: true
		},
		component: () =>
			import(/* webpackChunkName: "confirm_email" */ "../views/ConfirmEmail.vue")
	}
];

const router = new VueRouter({
  routes,
	mode: 'history'
});

export default router;
