import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./axios";
import './loading';
import './dialog';

import { ValidationProvider } from 'vee-validate'

Vue.component('ValidationProvider', ValidationProvider)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
