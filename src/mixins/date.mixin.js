import moment from 'moment'

export default {
	name: 'DateMixin',
	methods: {
		formatDate(date, format = 'DD MMM YYYY') {
			return moment(date).format(format)
		},
		formatDateForDatabase(date) {
			return this.formatDate(date, 'YYYY-MM-DD HH:mm:ss')
		}
	}
}