export default {
	name: 'LangMixin',
	computed: {
		lang() {
			let dic = this.$store.state.dictionary;

			if (!dic || !dic.hasOwnProperty(this.$store.state.lang)) {
				return false
			}
			return this.$store.state.dictionary[this.$store.state.lang]
		}
	},
	methods: {
		getDictionary() {
			let request = this.$http.get(process.env.VUE_APP_I18N_JS_URL, {
				withCredentials: true
			})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		}
	}
}