export default {
	methods: {
		showConfirm
		(
			callback,
			message = 'Are you sure?',
			options = {
				auth: false,
				message: 'Are you sure?',
				button: {
					no: 'Cancel',
					yes: 'Proceed'
				}
		}) {
			this.$vueConfirm.confirm(
					{
							...options,
							...{
								message: message
							}
					},
					callback
			)
		}
	},
	name: 'DialogMixin'
}