export default {
	name: 'TodoNotificationsMixin',
	methods: {
		getTodoNotifications(todoId) {
			let request = this.$http.get(`todo/${todoId}/notifications`)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		updateTodoNotification(todoId, notificationId, data = {}) {
			let request = this.$http.patch(`todo/${todoId}/notifications/${notificationId}`, data)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		}
	}
}