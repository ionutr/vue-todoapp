export default {
	methods: {
		getTodoTypes() {
			let request = this.$http.get(`todo/types`)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		}
	},
	name: 'TodoTypesMixin'
}