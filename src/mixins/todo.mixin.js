export default {
	methods: {
		completeTodo(id) {
			let request = this.$http.post(`todo/${id}/complete`, {})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		createTodo(data) {
			let request = this.$http.post(`todo`, data)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		deleteTodo(id) {
			let request = this.$http.delete(`todo/${id}`)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		getMyTodos(page = 1, withFullType = false) {
			let url = `todo?page=${page}`

			if (withFullType) {
				url += '&withType'
			}

			let request = this.$http.get(url)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		getTodo(id) {
			let request = this.$http.get(`todo/${id}`)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		restoreTodo(id) {
			let request = this.$http.post(`todo/${id}/restore`, {})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		updateTodo(id, data) {
			let request = this.$http.patch(`todo/${id}`, data)

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		}
	},
	name: 'TodosMixin'
}