import { toast } from 'bulma-toast'

export default {
	methods: {
		showToast(message, type = 'info', options = {
			closeOnClick: true,
			dismissible: true,
			duration: 2000,
			opacity: 1,
			pauseOnHover: false,
			position: 'top-right'
		}) {
			toast({
					...options,
					...{
						'message': message,
						'type': 'is-' + type
					}
			})
		}
	},
	name: 'ToasterMixin'
}