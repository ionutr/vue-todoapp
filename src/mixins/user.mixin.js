export default {
	methods: {
		confirmEmail(token) {
			let request = this.$http.post(`users/confirm/${token}`, {}, {
				withCredentials: true
			})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		getCurrentUser() {
			let request = this.$http.get(`users/me`, {
				withCredentials: true
			})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		},
		registerUser(data) {
			let request = this.$http.post(`users/register`, data, {
				withCredentials: true
			})

			return request
					.then(result => { return result })
					.catch(error => { throw error })
		}
	},
	name: 'UserMixin'
}